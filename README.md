# aceR - Elementary Cellular Automata with R

## Install

TODO

## Usage

See examples in `./examples/` directory.

You should use `library(aceR)` on the top of your script, and use one of the `ace.<something>` function this package provides.

Each function should be sufficiently documented, do not hesitate to ask for precision if any need.

## Author

- [Samuel ORTION](https://samuel.ortion.fr/)

## License 

This package is free software.
It is released under the GNU GPL v3+ license. See [./LICENSE](./LICENSE) for more details.
